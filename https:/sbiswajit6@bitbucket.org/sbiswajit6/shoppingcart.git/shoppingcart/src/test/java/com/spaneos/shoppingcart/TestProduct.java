package com.spaneos.shoppingcart;

import org.junit.Assert;
import org.junit.Test;

public class TestProduct {
	
	
	public void searchTest(){
		ShoppingDao sd= new ShoppingDaoImpl();
		Assert.assertEquals(1002, sd.search("Laptop"));
	}
	@Test
	public void getTest(){
		ShoppingDao sd= new ShoppingDaoImpl();
		Assert.assertEquals(2, sd.getById(1002).size());
	}
}
