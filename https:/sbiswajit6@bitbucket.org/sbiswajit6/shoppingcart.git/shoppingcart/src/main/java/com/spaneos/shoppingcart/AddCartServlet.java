package com.spaneos.shoppingcart;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddCartServlet
 */
@WebServlet("/AddCartServlet")
public class AddCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int pid=Integer.parseInt(request.getParameter("id"));
		HttpSession session=request.getSession(false);
		if(session==null){
			response.sendRedirect("index.jsp");
		}
		else{
			Object obj=session.getAttribute("sessioncartlist");
			if(obj==null){
				List<Product> cartlist=new ArrayList<>();
				//System.out.println(pid);
				ShoppingDao sd=new ShoppingDaoImpl();
				cartlist=sd.getById(pid);
				session.setAttribute("sessioncartlist", cartlist);
				/*for(Product i:cartlist){
					System.out.println("Product Price"+i.getPname());
				}*/
			}
			else{
				List<Product> cartlist=(List<Product>) obj;
				//System.out.println(pid);
				ShoppingDao sd=new ShoppingDaoImpl();
				cartlist.add(sd.getProduct(pid));
				/*for(Product i:cartlist){
					System.out.println("Product name"+i.getPname());
				}*/
				session.setAttribute("sessioncartlist", cartlist);
				
			}
			/*List<Product> list=new ArrayList<Product>();
			list=(List<Product>) session.getAttribute("sessioncartlist");
			for(Product i:list){
				System.out.println("Product name"+i.getPname());
			}*/
		}
		request.getRequestDispatcher("index.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
