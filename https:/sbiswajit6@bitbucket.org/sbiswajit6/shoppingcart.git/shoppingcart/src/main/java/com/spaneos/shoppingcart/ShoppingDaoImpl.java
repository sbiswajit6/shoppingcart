package com.spaneos.shoppingcart;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShoppingDaoImpl implements ShoppingDao{

	private ConnectionUtil dbUtil=ConnectionUtil.conUtilObj;
	private static final Logger LOGGER=LoggerFactory.getLogger(ShoppingDaoImpl.class);
	private static final String GET_PRODUCT="select * from product where CATEGORY_ID=?";
	private static final String GET_PRODUCTS="select * from product where PID=?";
	private static final String SEARCH_PRODUCTS = "select * from categories where CATEGORY_NAME=?";
	@Override
	public List<Product> getById(int pid) {
		Connection con = null;
		PreparedStatement st= null;
		ResultSet res=null;
		//List<Product> list = new ArrayList<Product>();
		List<Product> plist=new ArrayList<Product>();
		try {
			con = dbUtil.getConnection();
			st= con.prepareStatement(GET_PRODUCT);
			st.setInt(1, pid);
			//System.out.println(st);
			res= st.executeQuery();
			//System.out.println(res);
			while(res.next()){
				Product p = new Product();
				p.setPid(res.getInt("PID"));
				p.setPname(res.getString("P_NAME"));
				//System.out.println(res.getString("P_NAME"));
				p.setPdesc(res.getString("DESCRIPTION"));
				p.setPrice(res.getDouble("PRICE"));
				//System.out.println(p.getPrice());
				p.setDiscount(res.getInt("DISCOUNT"));
				//list.add(p);
				plist.add(p);
			}
			/*for(Product i:plist){
				System.out.println(i.getPrice());
				}*/
			//System.out.println(list);
		} catch (SQLException e) {
			LOGGER.info("While getiing : {}",e);
			e.printStackTrace();
		}finally{
			dbUtil.close(res, st, con);
		}
		return plist;
	}
	@Override
	public int search(String searchStr) {
		Connection con = null;
		PreparedStatement st= null;
		ResultSet res=null;
		int pid = 0;
		//List<Product> list = new ArrayList<>();
		try {
			con = dbUtil.getConnection();
			st= con.prepareStatement(SEARCH_PRODUCTS);
			st.setString(1, searchStr);
			//System.out.println(st);
			res= st.executeQuery();
			
			while(res.next()){
				//Product product = new Product();
				pid=res.getInt("CATEGORY_ID");
			}
			//System.out.println(pid);
		} catch (SQLException e) {
			LOGGER.info("While serching : {}",e);
		}finally{
			dbUtil.close(st, con);
		}
		return pid;
	}
	@Override
	public Product getProduct(int pid) {
		Connection con = null;
		PreparedStatement st= null;
		ResultSet res=null;
		//List<Product> list = new ArrayList<Product>();
		//List<Product> prolist=new ArrayList<Product>();
		Product p = new Product();
		try {
			con = dbUtil.getConnection();
			st= con.prepareStatement(GET_PRODUCTS);
			st.setInt(1, pid);
			//System.out.println(st);
			res= st.executeQuery();
			//System.out.println(res);
			
			while(res.next()){
				
				p.setPid(res.getInt("PID"));
				p.setPname(res.getString("P_NAME"));
				//System.out.println(res.getString("P_NAME"));
				p.setPdesc(res.getString("DESCRIPTION"));
				p.setPrice(res.getDouble("PRICE"));
				//System.out.println(p.getPrice());
				p.setDiscount(res.getInt("DISCOUNT"));
				//list.add(p);
				//prolist.add(p);
			}
			/*for(Product i:plist){
				System.out.println(i.getPrice());
				}*/
			//System.out.println(list);
		} catch (SQLException e) {
			LOGGER.info("While getiing : {}",e);
			e.printStackTrace();
		}finally{
			dbUtil.close(res, st, con);
		}
		return p;
	}
	

}
