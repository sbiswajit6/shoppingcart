package com.spaneos.shoppingcart;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CheckOutServlet
 */
@WebServlet("/CheckOutServlet")
public class CheckOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Cart c = null;
		List<Product> list = (List<Product>) session
				.getAttribute("sessioncartlist");
		List<Cart> tlist=new ArrayList<Cart>();
		int totalItem = list.size();
		double totalPrice = 0;
		double totalDiscount = 0;
		double netPrice = 0;
		for (Product i : list) {
			totalPrice = totalPrice + i.getPrice();
			double discountOnProduct = i.getPrice() * (i.getDiscount() / 100);
			totalDiscount = totalDiscount + discountOnProduct;
			double productDiscount = i.getPrice() - discountOnProduct;
			netPrice = netPrice + (productDiscount);
			
		}
		c = new Cart(totalItem, totalPrice, totalDiscount,
				netPrice);
			tlist.add(c);
		/*for(Cart i:tlist){
			
		}*/
		request.setAttribute("sessioncartlist", tlist);
		request.getRequestDispatcher("totalview.jsp").forward(request, response);
		//System.out.println(c.getTotalItem());
		//session.invalidate();
		//response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
