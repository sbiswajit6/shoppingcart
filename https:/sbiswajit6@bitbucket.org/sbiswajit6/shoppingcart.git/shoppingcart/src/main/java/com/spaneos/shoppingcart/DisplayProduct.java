package com.spaneos.shoppingcart;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class DisplayProduct
 */
@WebServlet("/DisplayProduct")
public class DisplayProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<Product> productlist=new ArrayList<>();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ShoppingDao sd=new ShoppingDaoImpl();
		String searchStr= request.getParameter("searchStr");
		//System.out.println(searchStr);
		///List<Product> searchResult = new ArrayList<Product>();
		int pid=sd.search(searchStr);
			productlist=sd.getById(pid);
			/*for(Product i:productlist){
			System.out.println("Product Price"+i.getPrice());
			}*/
		request.setAttribute("allproduct", productlist);
		RequestDispatcher rd=request.getRequestDispatcher("viewpro.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
