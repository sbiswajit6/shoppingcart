package com.spaneos.shoppingcart;

public class Cart {
	

	private int totalItem;
	private double totalPrice;
	private double totalDiscount;
	private double netPrice;

	public Cart(int totalItem, double totalPrice,
			double totalDiscount, double netPrice) {
		
		
		this.totalItem=totalItem;
		this.totalPrice=totalPrice;
		this.totalDiscount=totalDiscount;
		this.netPrice=netPrice;
	}


	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public double getNetPrice() {
		return netPrice;
	}

	public void setNetPrice(double netPrice) {
		this.netPrice = netPrice;
	}


	@Override
	public String toString() {
		return "Cart [totalItem=" + totalItem + ", totalPrice=" + totalPrice
				+ ", totalDiscount=" + totalDiscount + ", netPrice=" + netPrice
				+ "]";
	}

	
	
	

}
