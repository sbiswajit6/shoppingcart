package com.spaneos.shoppingcart;

import java.util.List;

public interface ShoppingDao {
	public List<Product> getById(int pid);

	public int search(String searchStr);

	public Product getProduct(int pid);
}
