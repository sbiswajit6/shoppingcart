<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <script src="https://code.jquery.com/jquery-3.1.0.js"
	integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="
	crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function() {
		$(".addcart").click(function() {
			//alert("ok")
			var id = $(this).attr('id')
			alert(id)
			$.ajax({
				url : "AddCartServlet",
				method : "get",
				data : {
					id : id
				},
				success : function(data) {
					alert("got id")
				}

			});
		});
	});
</script>
</head>
<body>
 <table class="table">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Desc.</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Buy</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach items="${allproduct}" var="p">
							<tr>
			
								<td>${p.pname}</td>
								<td>${p.pdesc}</td>
								<td>${p.price}</td>
								<td>${p.discount}</td>
								<td><a><span
						class="glyphicon glyphicon-shopping-cart addcart" id="${p.pid}"></span></a></td>
							</tr>
						</c:forEach>
    </tbody>
  </table>
</body>
</html>